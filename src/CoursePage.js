
import React from "react";


import {getCourseById} from './Service'

import AppToolbar from './AppToolbar'
import AppFooter from './AppFooter';


export default function CoursePage (props) {

    const id = props.match.params.id
    const course = getCourseById(id)
    return (<>
    <AppToolbar></AppToolbar>

    <h1>{course.title}</h1>

    <AppFooter></AppFooter>
    
    </>)
}