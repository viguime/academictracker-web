import React from 'react';

import Grid from '@material-ui/core/Grid';

import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';


import CardCourse from './CardCourse'

const useStyles = makeStyles(theme => ({
    icon: {
      marginRight: theme.spacing(2),
    },
    heroContent: {
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
      marginTop: theme.spacing(4),
    },
    cardGrid: {
      paddingTop: theme.spacing(8),
      paddingBottom: theme.spacing(8),
    },
    card: {
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    cardMedia: {
      paddingTop: '56.25%', // 16:9
    },
    cardContent: {
      flexGrow: 1,
    },
    footer: {
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing(6),
    },
  }));

export default function CourseList (props) {

    const classes = useStyles();
    
    return  (
        <Container className={classes.cardGrid} maxWidth="md">
        {/* End hero unit */}

        <Grid container spacing={4}>
        {props.courses.map(course => (

          <Grid  item key={course._id} xs={12} sm={6} md={4}>

            <CardCourse  course={course}></CardCourse>

        </Grid>

        )
        
        )}
      </Grid>
    </Container>
      

    )

}