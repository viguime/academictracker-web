import database from "./database.json"

export const getCourses = () => Object.values(database.courses)
export const getCourseById = (id) => database.courses[id]

